const Users = require('../model/userModel')

const userControllers = {
  async addUser (req,res) {
    const user = req.body
    try{
      const addedUser = new Users (user)
      addedUser.save()
      res.json(addedUser)
    }
    catch(err){
      res.status(500).json(err)
    }
  },
  async updateUser (req,res) {
    const user = req.body
    try{
      const updateUser = await Users.updateOne({_id: user._id},user)
      res.json(updateUser)
    }catch(err){
      res.status(500).json(err)
    }

  },
  async deleteUser (req,res) {
    const {id} = req.params
    try{
      const deleteUser = await Users.deleteOne({_id:id})
       res.json(deleteUser)
    }catch(err){
      res.status(500).json(err)
    }
  },
  async getUsers(req,res){
    try{
      const users = await Users.find()
      res.json(users)
    }catch(err){
      res.status(500).json(err)
    }
  },
  async getUserById(req,res){
    const user = req.params
    try{
      const getUser = await Users.findById(user)
      res.json(getUser)
    }catch(err){
      res.status(500).json(err)
    }
  }
}

module.exports = userControllers
