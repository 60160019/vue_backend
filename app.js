var express = require('express');
const cors = require('cors')
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose')
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');


// Database connected
mongoose.connect('mongodb://admin:password@localhost/mydb', { useNewUrlParser: true,useUnifiedTopology:true })
const db = mongoose.connection
db.on('error', console.error.bind(console, 'Connection Error '))
db.once('open', function () {
  console.log('Connected to database default port:8081')
})

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors())

app.use('/', indexRouter);
app.use('/users', usersRouter);

module.exports = app;
