/* eslint-disable no-unused-vars */
var express = require('express')
var router = express.Router()
const userControllers = require('../controllers/userController')
/* GET users listing. */

router.get('/', userControllers.getUsers);
router.get('/:id',userControllers.getUserById)
router.post('/',userControllers.addUser);
router.put('/',userControllers.updateUser)
router.delete('/:id',userControllers.deleteUser)



module.exports = router
